package com.demo.publisher.eventBus;

import com.demo.publisher.subscruber.OrderProcessSubscriber;
import com.demo.publisher.subscruber.PaymentSubscriber;
import com.google.common.eventbus.AsyncEventBus;
import java.util.concurrent.Executors;
import org.springframework.stereotype.Component;

@Component
public class Bus {
  public static AsyncEventBus event =
      new AsyncEventBus("event-bus", Executors.newFixedThreadPool(5));

  public Bus(OrderProcessSubscriber orderProcessSubscriber, PaymentSubscriber paymentSubscriber) {
    event.register(orderProcessSubscriber);
    event.register(paymentSubscriber);
  }

  public void publishEvent(String message) {
    event.post(message);
  }

}
