package com.demo.publisher.eventBus;

import com.demo.publisher.subscruber.OrderResultSubscriber;
import com.google.common.eventbus.AsyncEventBus;
import java.util.concurrent.Executors;
import org.springframework.stereotype.Component;

@Component
public class BusResult {
  public static AsyncEventBus event1 =
      new AsyncEventBus("event-bus-result", Executors.newFixedThreadPool(5));

  public BusResult(OrderResultSubscriber orderResultSubscriber){
    event1.register(orderResultSubscriber);
  }
  public void publishEventResult(String message){
    event1.post(message);
  }
}
