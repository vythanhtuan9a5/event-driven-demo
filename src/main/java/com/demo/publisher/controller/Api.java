package com.demo.publisher.controller;

import com.demo.publisher.entity.Order;
import com.demo.publisher.repository.StoreRepository;
import com.demo.publisher.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {

  @Autowired
  private StoreRepository repository;

  @Autowired
  private OrderService orderService;
  @GetMapping("api/v1/post")
  public Order createOrder(String name){
      return orderService.create(name);
  }

  @GetMapping("api/v1/get")
  public Order getOrder(String id){
    return repository.getOrder(id);
  }

}
