package com.demo.publisher.entity;

import com.demo.publisher.enums.OrderState;
import java.util.ArrayList;
import java.util.List;

public class Order {
  String name;
  int quality;
  String id;
  OrderState state = OrderState.CREATED;
  List<OrderStep> steps = new ArrayList<>();

  public OrderState getState() {
    return state;
  }

  public void setState(OrderState state) {
    this.state = state;
  }

  public List<OrderStep> getSteps() {
    return steps;
  }

  public void setSteps(List<OrderStep> steps) {
    this.steps = steps;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getQuality() {
    return quality;
  }

  public void setQuality(int quality) {
    this.quality = quality;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
