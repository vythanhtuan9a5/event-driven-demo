package com.demo.publisher.entity;

import com.demo.publisher.enums.StepState;

public class OrderStep {
  String name;
  String description;
  String error;
  Integer errorCode;
  StepState stepState;

  public OrderStep() {}

  public static OrderStep paymentStep(Order order) {
    OrderStep step = new OrderStep();
    step.setStepState(StepState.PENDING);
    step.setName("Payment-process");
    step.setDescription("");
    step.setError("");
    step.setErrorCode(0);
    return step;
  }

  public static OrderStep orderStep(Order order) {
    OrderStep step = new OrderStep();
    step.setStepState(StepState.PENDING);
    step.setName("order-process");
    step.setDescription("");
    step.setError("");
    step.setErrorCode(0);
    return step;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public Integer getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  public StepState getStepState() {
    return stepState;
  }

  public void setStepState(StepState stepState) {
    this.stepState = stepState;
  }
}
