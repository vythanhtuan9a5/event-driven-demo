package com.demo.publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class PublisherApplication {

  public static void main(String[] args) {
    SpringApplication.run(PublisherApplication.class, args);
  }

  //		@Bean
  //		public ProducerFactory<String, String> producerFactory() {
  //			Map<String, Object> configProps = new HashMap<>();
  //			configProps.put(
  //					ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
  //					"localhost:9092");
  //			configProps.put(
  //					ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
  //					StringSerializer.class);
  //			configProps.put(
  //					ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
  //					StringSerializer.class);
  ////			configProps.put(ProducerConfig.LINGER_MS_CONFIG,10000);
  //			//all ,-1,0,1 all
  //
  //			return new DefaultKafkaProducerFactory<>(configProps);
  //		}
  //
  //		@Bean
  //		public KafkaTemplate<String, String> kafkaTemplate() {
  //			return new KafkaTemplate<>(producerFactory());
  //		}

}
