package com.demo.publisher.enums;

public enum StepState {
  PENDING,
  PROCESSING,
  SUCCESS,
  FAILED
}
