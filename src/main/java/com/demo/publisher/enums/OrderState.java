package com.demo.publisher.enums;

public enum OrderState {
  CREATED, PROCESSING, SUCCESS, FAILED
}
