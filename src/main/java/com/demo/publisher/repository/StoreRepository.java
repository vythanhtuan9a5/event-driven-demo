package com.demo.publisher.repository;

import com.demo.publisher.entity.Order;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class StoreRepository {
  private static Map<String, Integer> store = new HashMap<>();
  private static Map<String, Order> orderStore = new HashMap<>();

  static {
    store.put("audi", 2);
    store.put("honda", 1);
  }

  public boolean buy(String name) {
    int quality = store.getOrDefault(name, 0);
    return quality > 0;
  }

  public Order createOrder(String name) {
    Order order = new Order();
    order.setName(name);
    order.setQuality(1);
    order.setId(UUID.randomUUID().toString());
    orderStore.put(order.getId(), order);
    return order;
  }

  public void updateOrder(Order order) {
    orderStore.put(order.getId(), order);
  }

  public Order getOrder(String id) {
    return orderStore.get(id);
  }
}
