package com.demo.publisher.subscruber;

import com.demo.publisher.service.OrderService;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderResultSubscriber {
  @Autowired private OrderService orderService;

  @Subscribe
  public void listener(String orderId) {
    System.out.println(Thread.currentThread().getName() + "-OrderResult");
    orderService.updateOrderResult(orderId);
  }
}
