package com.demo.publisher.subscruber;

import com.demo.publisher.eventBus.Bus;
import com.demo.publisher.eventBus.BusResult;
import com.demo.publisher.service.PaymentService;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentSubscriber {
  @Autowired
  private PaymentService paymentService;
  @Autowired
  private BusResult bus;
  @Subscribe
  public void listener(String orderId){
    System.out.println(Thread.currentThread().getName()+"-paymentProcess");
    paymentService.processPayment(orderId);
    bus.publishEventResult(orderId);
  }
}
