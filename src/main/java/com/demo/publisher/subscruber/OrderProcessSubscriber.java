package com.demo.publisher.subscruber;

import com.demo.publisher.eventBus.Bus;
import com.demo.publisher.eventBus.BusResult;
import com.demo.publisher.service.OrderService;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderProcessSubscriber {
  @Autowired
  private OrderService orderService;
  @Autowired
  private BusResult bus;
  @Subscribe
  public void listener(String orderId) {
    System.out.println(Thread.currentThread().getName()+"-orderProcess");
    orderService.processOrder(orderId);
    bus.publishEventResult(orderId);
  }
}
