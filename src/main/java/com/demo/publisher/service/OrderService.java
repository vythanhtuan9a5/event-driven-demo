package com.demo.publisher.service;

import com.demo.publisher.entity.Order;
import com.demo.publisher.entity.OrderStep;
import com.demo.publisher.enums.OrderState;
import com.demo.publisher.enums.StepState;
import com.demo.publisher.eventBus.Bus;
import com.demo.publisher.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
  @Autowired private StoreRepository repository;
  @Autowired private Bus bus;

  public Order create(String name) {
    Order order = repository.createOrder(name);
    OrderStep paymentStep = OrderStep.paymentStep(order);
    OrderStep processStep = OrderStep.orderStep(order);
    order.getSteps().add(processStep);
    order.getSteps().add(paymentStep);
    order.setState(OrderState.PROCESSING);
    repository.updateOrder(order);
    bus.publishEvent(order.getId());
    return order;
  }

  public void processOrder(String orderId) {
    Order order = repository.getOrder(orderId);
    order.getSteps().get(0).setStepState(StepState.PROCESSING);
    System.out.println("process order process ");
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    if (order.getName().equals("abc")) {
      order.getSteps().get(0).setStepState(StepState.FAILED);
      order.getSteps().get(0).setError("Name not found");
      order.getSteps().get(0).setErrorCode(100001);
    } else {
      order.getSteps().get(0).setStepState(StepState.SUCCESS);
    }
    repository.updateOrder(order);
  }

  public void updateOrderResult(String orderId) {
    Order order = repository.getOrder(orderId);
    boolean isPending =
        order.getSteps().stream()
            .anyMatch(
                orderStep ->
                    orderStep.getStepState().equals(StepState.PENDING)
                        || orderStep.getStepState().equals(StepState.PROCESSING));
    if (!isPending) {
      boolean isSuccess =
          order.getSteps().stream()
              .allMatch(orderStep -> orderStep.getStepState().equals(StepState.SUCCESS));
      if (isSuccess) order.setState(OrderState.SUCCESS);
      else order.setState(OrderState.FAILED);
      repository.updateOrder(order);
    }
  }
}
