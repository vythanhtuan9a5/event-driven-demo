package com.demo.publisher.service;

import com.demo.publisher.entity.Order;
import com.demo.publisher.enums.StepState;
import com.demo.publisher.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

  @Autowired private StoreRepository repository;

  public void processPayment(String orderId) {
    Order order = repository.getOrder(orderId);
    order.getSteps().get(1).setStepState(StepState.PROCESSING);
    System.out.println("process payment success");
    try {
      Thread.sleep(7000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    order.getSteps().get(1).setStepState(StepState.SUCCESS);
    repository.updateOrder(order);
  }
}
