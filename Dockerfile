FROM adoptopenjdk/openjdk11:jdk-11.0.2.7-alpine-slim
USER root
ADD /target/*.jar app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]
ENTRYPOINT ["sh", "-c", "java -Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2 -jar /app.jar"]